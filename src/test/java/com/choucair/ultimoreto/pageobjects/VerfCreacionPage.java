package com.choucair.ultimoreto.pageobjects;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class VerfCreacionPage extends PageObject {
	@FindBy(xpath = "//div[@id='slickgrid_692386UnitId']//span[contains(text(),'ID')]")
	public WebElementFacade verifCre;
	@FindBy(xpath = "//div[contains(@class,'ui-widget-content slick-row even')]")
	public WebElementFacade verifCre3;
	@FindBy(xpath = "//div[@class='ui-widget-content slick-row even']//child::div[@class='slick-cell l1 r1']")
	public WebElementFacade verifCre2;
	
}