package com.choucair.ultimoreto.pageobjects;

import org.openqa.selenium.support.FindBy;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

public class CrearMeetPage extends PageObject {
	@FindBy(xpath = "//i[contains(@class,'nav-icon fa fa-comments premium-feature')]")
	public WebElementFacade bnmenu;
	@FindBy(xpath = "//a[contains(@href,'/demo/Meeting/Meeting')]")
	public WebElementFacade bnmenu2;
	@FindBy(xpath = "//span[contains(text(),'New Meeting')]")
	public WebElementFacade bnnewm;
	@FindBy(name = "MeetingName")
	public WebElementFacade mName;
	@FindBy(id = "s2id_StartSharp_Meeting_MeetingDialog14_MeetingTypeId")
	public WebElementFacade mtype;
	@FindBy(id = "select2-results-6")
	public WebElementFacade mtype2;
	@FindBy(id = "StartSharp_Meeting_MeetingDialog14_StartDate")
	public WebElementFacade mfecha;
	@FindBy(xpath = "//select[@class='editor s-DateTimeEditor time valid']")
	public WebElementFacade mhora;
	@FindBy(xpath = "//select[contains(@class,'editor s-DateTimeEditor time valid')]/option[68]")
	public WebElementFacade mhora2;
	@FindBy(xpath = "//div[@class='field LocationId col-sm-6']//child::a[@class='inplace-button inplace-create']")
	public WebElementFacade btubi;
	@FindBy(xpath = "//input[@id='StartSharp_Meeting_MeetingLocationDialog37_Name']")
	public WebElementFacade lname;
	@FindBy(xpath = "//input[@id=('StartSharp_Meeting_MeetingLocationDialog37_Address')]")
	public WebElementFacade lubi;
	@FindBy(xpath = "//input[@id=('StartSharp_Meeting_MeetingLocationDialog37_Latitude')]")
	public WebElementFacade llati;
	@FindBy(xpath = "//input[@id=('StartSharp_Meeting_MeetingLocationDialog37_Longitude')]")
	public WebElementFacade llon;
	@FindBy(xpath = "//div[@id='StartSharp_Meeting_MeetingLocationDialog37_Toolbar']//child::i[@class=('fa fa-floppy-o text-purple')]")
	public WebElementFacade lSave;
	@FindBy(xpath = "//div[@class='field OrganizerContactId col-sm-6']//child::a[@class='inplace-button inplace-create']")
	public WebElementFacade btubi2;
	@FindBy(id = "StartSharp_Organization_ContactDialog44_Title")
	public WebElementFacade otitle;
	@FindBy(id = "StartSharp_Organization_ContactDialog44_FirstName")
	public WebElementFacade oname;
	@FindBy(id = "StartSharp_Organization_ContactDialog44_LastName")
	public WebElementFacade olast;
	@FindBy(id = "StartSharp_Organization_ContactDialog44_Email")
	public WebElementFacade oemail;
	@FindBy(className = "emaildomain")
	public WebElementFacade oemail2;
	@FindBy(id = "StartSharp_Organization_ContactDialog44_IdentityNo")
	public WebElementFacade ouserid;
	@FindBy(xpath = "//div[@id='StartSharp_Organization_ContactDialog44_Toolbar']//child::i[@class=('fa fa-floppy-o text-purple')]")
	public WebElementFacade lSave2;
	@FindBy(id = "s2id_autogen11")
	public WebElementFacade alist;
	@FindBy(id = "select2-result-label-16")
	public WebElementFacade alist2;
	@FindBy(id = "StartSharp_Meeting_MeetingDialog14_MeetingNumber")
	public WebElementFacade meetnro;
	@FindBy(id = "StartSharp_Meeting_MeetingDialog14_EndDate")
	public WebElementFacade efecha;
	@FindBy(id = "StartSharp_Meeting_MeetingDialog10_EndDate")
	public WebElementFacade nmeet;
	@FindBy(id = "select2-chosen-8")
	public WebElementFacade unit;
	@FindBy(id = "s2id_autogen8_search")
	public WebElementFacade unit2;
	@FindBy(id = "select2-results-8")
	public WebElementFacade unit3;
	@FindBy(id = "select2-chosen-10")
	public WebElementFacade alist3;
	@FindBy(id = "select2-results-10")
	public WebElementFacade alist4;
	@FindBy(xpath = "//span[contains(text(),'Save')]")
	public WebElementFacade bnSave;
	@FindBy(xpath = "//div[contains(@class,'ui-state-default slick-header-column slick-header-sortable ui-sortable-handle')]")
	public WebElementFacade vf;
	@FindBy(xpath = "//span[contains(@class,'slick-sort-indicator slick-sort-indicator-asc')]")
	public WebElementFacade vf2;
	@FindBy(xpath = "//div[contains(@class,'slick-cell l1 r1')]")
	public WebElementFacade vf3;
	@FindBy(xpath = "//div[contains(@class,'slick-cell l7 r7')]")
	public WebElementFacade vf4;	
}
