package com.choucair.ultimoreto.steps;

import java.util.List;
import com.choucair.ultimoreto.pageobjects.CrearMeetPage;
import com.choucair.ultimoreto.utilities.AccionesWeb;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class CrearMeetSteps {
	AccionesWeb accionesWeb;
	CrearMeetPage crearMeetPage;

	@Step
	public void crearMeet(List<List<String>> data, int id) {
		accionesWeb.espBordearClick(crearMeetPage.bnmenu);
		accionesWeb.bordearClick(crearMeetPage.bnmenu2);
		
		accionesWeb.espBordearClick(crearMeetPage.bnnewm);
		
		accionesWeb.espBordearClick(crearMeetPage.mName);
		String VarBuss = Serenity.sessionVariableCalled("Var Bussi");
		crearMeetPage.mName.sendKeys(VarBuss);
		accionesWeb.bordearClick(crearMeetPage.mtype);
		accionesWeb.bordearClick(crearMeetPage.mtype2);
		accionesWeb.bordearElemento(crearMeetPage.mfecha);
		accionesWeb.clear_sendKeys(crearMeetPage.mfecha, data.get(id).get(0).trim());
		//Location
		accionesWeb.bordearClick(crearMeetPage.btubi);
		accionesWeb.espBorCliSendKey(crearMeetPage.lname, data.get(id).get(1).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.lubi, data.get(id).get(2).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.llati, data.get(id).get(3).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.llon, data.get(id).get(4).trim());
		accionesWeb.bordearScrenClick(crearMeetPage.lSave);
		//Contact
		accionesWeb.espBordearClick(crearMeetPage.btubi2);
		accionesWeb.espBorCliSendKey(crearMeetPage.otitle, data.get(id).get(5).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.oname, data.get(id).get(6).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.olast, data.get(id).get(7).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.oemail, data.get(id).get(8).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.oemail2, data.get(id).get(9).trim());
		accionesWeb.espBorCliSendKey(crearMeetPage.ouserid, data.get(id).get(10).trim());
		accionesWeb.bordearScrenClick(crearMeetPage.lSave2);
		
		accionesWeb.espBordearClick(crearMeetPage.alist);
		accionesWeb.espBordearClick(crearMeetPage.alist2);
		accionesWeb.espBorCliSendKey(crearMeetPage.meetnro, data.get(id).get(10).trim());
		accionesWeb.bordearElemento(crearMeetPage.efecha);
		accionesWeb.clear_sendKeys(crearMeetPage.efecha, data.get(id).get(11).trim());
		accionesWeb.espBordearClick(crearMeetPage.unit);
		accionesWeb.clear_sendKeys(crearMeetPage.unit2, VarBuss);
		accionesWeb.click(crearMeetPage.unit3, false);
		accionesWeb.espBordearClick(crearMeetPage.alist3);//reporter
		accionesWeb.espBordearClick(crearMeetPage.alist4);
		accionesWeb.bordearScrenClick(crearMeetPage.bnSave);
	}
}
