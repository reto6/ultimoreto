package com.choucair.ultimoreto.steps;

import com.choucair.ultimoreto.pageobjects.CrearMeetPage;
import com.choucair.ultimoreto.pageobjects.VerfCreacionPage;
import com.choucair.ultimoreto.utilities.AccionesWeb;

import net.thucydides.core.annotations.Step;

public class VerfCreacionSteps {
	VerfCreacionPage verfCreacionDefinitions;
	AccionesWeb accionesWeb;
	CrearMeetPage crearMeetPage;
	
	@Step
	public void validar() {
		accionesWeb.esperoElementoVisible(verfCreacionDefinitions.verifCre2);
		accionesWeb.bordearValdScren(verfCreacionDefinitions.verifCre2);
	}
	public void validar2() {
		accionesWeb.esperoElementoVisible(crearMeetPage.bnnewm);
		accionesWeb.click(crearMeetPage.vf, false);
		accionesWeb.click(crearMeetPage.vf2, false);
		accionesWeb.esperoElementoVisible(crearMeetPage.vf2);
		//accionesWeb.bordearEspe2(crearMeetPage.vf3);
		accionesWeb.bordearValdScren2(crearMeetPage.vf3, crearMeetPage.vf4);
		accionesWeb.valTextoDeUnElemetoContieneTexto(crearMeetPage.vf3, crearMeetPage.vf4);
	}
	
}
