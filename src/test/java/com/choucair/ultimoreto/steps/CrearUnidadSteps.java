package com.choucair.ultimoreto.steps;

import java.util.List;

import org.apache.pdfbox.contentstream.operator.state.Concatenate;
import org.yecht.Data.Str;

import com.choucair.ultimoreto.pageobjects.CrearUnidadPage;
import com.choucair.ultimoreto.utilities.AccionesWeb;

import net.serenitybdd.core.Serenity;
import net.thucydides.core.annotations.Step;

public class CrearUnidadSteps {
	AccionesWeb accionesWeb;
	CrearUnidadPage crearUnidadDefinitions;

	@Step
	public void menu() {
		accionesWeb.espBordearClick(crearUnidadDefinitions.bnunidadn);
		accionesWeb.bordearClick(crearUnidadDefinitions.bnunidadn2);
		accionesWeb.espBordearClick(crearUnidadDefinitions.newbuss);
		Serenity.setSessionVariable("Var Bussi").to("Evento Empresarial"+accionesWeb.aleatorio());
		String VarBuss = Serenity.sessionVariableCalled("Var Bussi");
		accionesWeb.espBorCliSendKey(crearUnidadDefinitions.bussname, VarBuss);
		//accionesWeb.espBorCliSendKey(crearUnidadDefinitions.bussname, accionesWeb.aleatorio());
		accionesWeb.bordearClickScren(crearUnidadDefinitions.bnSave);
		
	}
}
